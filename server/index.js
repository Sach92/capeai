const express = require("express");
const cors = require('cors');
const redis = require("redis");
const process = require("process");

const app = express();
app.use(cors());

const client = redis.createClient({
  host: "redis-server",
  port: 6379
});
client.set("name", "Kuziwa Sachikonye");

app.get("/", (req, res) => {
  //process.exit(0)
  client.get("name", (err, name) => {
    res.send({"name" : name});
  });
});

app.listen(8081, () => {
  console.log("listening on port 8081");
});
