# Case Questions

***1. Choosing one of the biggest cloud providers (AWS / GCP or Azure) how would you deploy your docker webapp to one of these providers? Be as  specific as possible.***

### **Deploying to AWS** 

On AWS, there is a choice of managed container services to use, ECS, ElasticBeanStalk and EKS. I nominate ECS because it is the simplest of the 3 when translating a docker-compose into a production environment. The process of deploying to ECS is documented [here](https://aws.amazon.com/getting-started/hands-on/deploy-docker-containers/) on AWS

> ***NOTE:*** *there are some costs associated with running the environments for hosting the web-app. While these costs tend to be very minor, they can accumulate if such and instance is created and left running without closing it down. Please do not forget to shut down your environment once you have completed.*

***2. Would your above answer change if the constraint was added that your docker images are not allowed to be hosted in an online image registry (like Dockerhub) but have to be pushed to the instance via SSH or something similar?***

* The answer above will not change. To switch from public to private images, I would recommend the use of ECR private registries. To enable ECS to pull from private ECR registries, we first have to create read & write IAM roles and permissions to ECR and attach them to ECS. This is a simpler implementation using managed services.

***3. Imagine your hosted web application is receiving a lot more traffic than expected, how would you go about scaling your web application so it can handle a greater volume of internet traffic?***

* The ECS service comes built-in with auto scaling features, once the scaling requirements have been determined, then the metrics are adjusted to ensure scaling is performed according to the desired traffic level.

***4. A prospective client wants a web application for his store that allows users to sign up and order his goods. The idea is that later on our Cape AI superhero data scientists will perform data analytics on consumer behaviour so the data has to be relationally structured.***

***The client has the idea that he will have users sign up with their email, and will also ask for the following: email, phone, age, name and address.***

***He also wants all his product information to be stored in the same database, a product has a name, description and price***

***For his orders he would like to have an order date, total price, the products that were ordered, and he would like to keep track of whether an order has been paid.***

***Given the above information, make an ERD (Entity Relationship Diagram) of the envisioned database with an explanation to some of your design decisions. What did you consider over option A?***

![E-Commerce ERD Diagram](capeaiq5.png)
*Fig. 3: Client E-Commerce Store ERD Diagram*

### **Database Design Decisions**
1. **Order_Product Table:** A many-many relationship exists between the orders and the products. This means that whilst an order may contain many products, likewise a product can be found in many different orders. This relationship is normalized by having the `order_product_quantity` table, wherein the `order_id` and the `order_product_quantity_id` both exist as foriegn keys (FK).
    * To determine the products that are part of an order, this table merely needs to be queried, with the `order_id` as the `WHERE` clause, to return all the products associated in a particular order.
    * Likewise if this table is queried with the `order_product_quantity_id` as the `WHERE` clause, the orders that a particular product is associated with will be returned.
2. **Order.total_price:** The total price for a given order is a computed value that is obtained from the sum of the prices of the products that are present in the respective order
3. **PK Auto-Increment:** The database is expected to have unique identifiers for each customer, order and product. This is ensured by having the *auto-increment* tag on each of the ids.
4. **Constraints:** The constraints imposed on each of the fields was a matter of best judgement and [research](https://webarchive.nationalarchives.gov.uk/+/http://www.cabinetoffice.gov.uk/media/254290/GDS%20Catalogue%20Vol%202.pdf)
5. **Order.is_paid Default:** The `Order.is_paid` field was set as a default of `0` to ensure that when an order is created, it is first unpaid and must then be updated to (`paid 1`)

***
## **Assignment Statistics**
* Assignment Status : `COMPLETE`
* Productive Time Elapsed : ~ 6:00:00 (6hrs)