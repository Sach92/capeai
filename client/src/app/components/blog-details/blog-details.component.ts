import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProfileService } from 'src/app/services/profile/profile.service';

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.scss']
})
export class BlogDetailsComponent implements OnInit {

  name$ : Observable<any>;

  constructor(private profileService : ProfileService) {
    this.name$ = profileService.getName();    
   }

  ngOnInit() {
  }

}
