import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PreloaderComponent } from './components/common/preloader/preloader.component';
import { NavbarComponent } from './components/common/navbar/navbar.component';
import { AboutComponent } from './components/common/about/about.component';
import { FooterComponent } from './components/common/footer/footer.component';
import { BlogComponent } from './components/common/blog/blog.component';
import { HomeFourComponent } from './components/home-four/home-four.component';
import { BlogDetailsComponent } from './components/blog-details/blog-details.component';
import { ProfileService } from './services/profile/profile.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [
        AppComponent,
        PreloaderComponent,
        NavbarComponent,
        AboutComponent,
        FooterComponent,
        BlogComponent,
        HomeFourComponent,
        BlogDetailsComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule
    ],
    providers: [ProfileService],
    bootstrap: [AppComponent]
})
export class AppModule { }