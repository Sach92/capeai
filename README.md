# Cape AI Application Assessment

Welcome to my submission for the Cape AI Assessment. This is a simple web-app that is designed to showcase my skills in web-design and understanding of containers as well as some cloud concepts.

## Features
The main features of the web-app are the biography and the joke page. From the landing page: 
* Clicking the "About Me" button will navigate the user to my [personal website](http://sach.co.za/).
* Clicking the "Jokes" button will navigate the user to the page showing a funny joke and displaying my name as retireved from the redis database.

## Special Effects

* I hope that you enjoy some of the extra effects in the application: 
    * **Ripple Animation:** A water effect when moving the cursor over the banner images.
    * **Animated Text:** The data that is [returned](http://localhost:4001/) from the redis database is displayed in the application using an animation effect. 

## Running The Application 
*   The application can be run using a single command

``docker-compose up``

### Case Questions
* The case quesitons are answered [here](/questions/README.md)